<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function display()
    {
        $title ="About Us";
        return view('about', compact('title'));
    }
}
