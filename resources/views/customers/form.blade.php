<div class="card-body">
    <div class="form-group row">
        <label class="col-md-3 text-right
        control-label col-form-label">Customer</label>
    </div>

    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right
            control-label col-form-label">Name</label>
        <div class="col-sm-9">
                <input type="text" name="name" value="{{ old('name') ?? $customers->name}}"
                class="form-control" id="name"
                placeholder="Customer name here">
        </div>
    </div>    

        <div class="form-group row">
            <label for="address" class="col-sm-3 text-right
            control-label col-form-label">Address</label>
            <div class="col-sm-9">
                <input type="text" name="address" 
                class="form-control" id="address" value="{{ old('address') ?? $customers->address}}"
                placeholder="Customer Address here">
        </div>
    </div>
</div>
@csrf